#! /bin/bash

set -euxo pipefail

cd -- "$(dirname "$0")"
scp -O target/riscv64gc-unknown-linux-musl/release/helloworld root@192.168.42.1:/root/
