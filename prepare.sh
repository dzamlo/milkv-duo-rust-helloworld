#! /bin/bash

set -euxo pipefail

cd -- "$(dirname "$0")"
rustup toolchain install nightly
rustup component add rust-src --toolchain nightly
if [[ -d ./duo-sdk ]]; then
    pushd duo-sdk
    git pull
    popd
else
    # sometimes it works with HTTP, sometime it works with SSH, so try both
    git clone https://github.com/milkv-duo/duo-sdk.git || git clone git@github.com:milkv-duo/duo-sdk.git
fi
