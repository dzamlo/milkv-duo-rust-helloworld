# Rust helloworld on a Milk-V Duo board

The goal of this repo is to build a rust executable and run it on the Linux side (big core) of a [Milk-V Duo](https://milkv.io/duo) board.

This is only tested on Linux.

## Step 0: clone this repo

```
git clone https://gitlab.com/dzamlo/milkv-duo-rust-helloworld.git
cd milkv-duo-rust-helloworld
```

## Step 1: install rustup

Follow instruction on https://rustup.rs/

## Step 2: install dependencies

Run the script:

```sh
./prepare.sh
```

## Step 3: build the executable

Run the script:

```sh
./build.sh
```

The resulting binary is at `target/riscv64gc-unknown-linux-musl/release/helloworld`.

## Step 4: upload the executable

Connect to the board via USB-C. If you connect by another mean, adapt the address in the script.

Run the script:

```sh
./upload.sh
```

## Step 5: run the executable:

Connect to board via SSH and run the executable:
```sh
ssh root@192.168.42.1
./helloworld
```

