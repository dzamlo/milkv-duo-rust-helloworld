#! /bin/bash

set -euxo pipefail

cd -- "$(dirname "$0")"
export PATH="./duo-sdk/riscv64-linux-musl-x86_64/bin:${PATH}"
cargo +nightly build --release
